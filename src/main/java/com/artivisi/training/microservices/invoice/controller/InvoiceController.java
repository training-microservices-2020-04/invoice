package com.artivisi.training.microservices.invoice.controller;

import com.artivisi.training.microservices.invoice.dao.InvoiceDao;
import com.artivisi.training.microservices.invoice.entity.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {
    @Autowired private InvoiceDao invoiceDao;

    @GetMapping("/")
    public Page<Invoice> findAll(Pageable pageable) {
        return invoiceDao.findAll(pageable);
    }
}
