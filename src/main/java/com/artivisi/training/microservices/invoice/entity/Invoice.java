package com.artivisi.training.microservices.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity @Data
public class Invoice {
    @Id
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Size(min = 3, max = 100)
    private String invoiceNumber;

    @NotNull @NotEmpty @Size(min = 3, max = 100)
    private String salesNumber;

    @NotNull
    private LocalDateTime salesTime;

    @NotNull @NotEmpty @Size(min = 3, max = 100)
    private String customerName;

    @NotNull @NotEmpty @Size(min = 3, max = 50)
    private String customerEmail;

    @NotNull
    private BigDecimal amount;

    private String description;

    @NotNull
    private LocalDateTime invoiceTime = LocalDateTime.now();

    @NotNull
    private LocalDate dueDate = LocalDate.now().plusDays(2);

    @NotNull @Enumerated(EnumType.STRING)
    private InvoiceStatus invoiceStatus = InvoiceStatus.UNPAID;
}
