package com.artivisi.training.microservices.invoice.service;


import com.artivisi.training.microservices.invoice.dao.InvoiceDao;
import com.artivisi.training.microservices.invoice.dto.InvoiceRequest;
import com.artivisi.training.microservices.invoice.entity.Invoice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Service @Transactional
public class InvoiceService {
    @Autowired private InvoiceDao invoiceDao;

    public Invoice generateInvoice(InvoiceRequest invoiceRequest) {
        Invoice invoice = new Invoice();
        BeanUtils.copyProperties(invoiceRequest, invoice);
        invoice.setInvoiceNumber(generateInvoiceNumber());
        invoiceDao.save(invoice);
        return invoice;
    }

    private String generateInvoiceNumber() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString().toUpperCase();

        String today = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)
                .replace("-", "");

        return "INV-"+today+"-"+generatedString;
    }
}
