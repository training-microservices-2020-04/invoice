package com.artivisi.training.microservices.invoice.service;

import com.artivisi.training.microservices.invoice.dto.InvoiceRequest;
import com.artivisi.training.microservices.invoice.entity.Invoice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service @Slf4j
public class KafkaListenerService {

    @Autowired private ObjectMapper objectMapper;
    @Autowired private InvoiceService invoiceService;

    @KafkaListener(topics = "${kafka.topic.invoice.request}",
            autoStartup = "${kafka.listen.auto.start:true}")
    public void handleInvoiceRequest(String msg) {

        try {
            log.info("Request : {}", msg);
            InvoiceRequest request = objectMapper.readValue(msg, InvoiceRequest.class);
            Invoice invoice = invoiceService.generateInvoice(request);
            log.info("Invoice : {}", invoice);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }

    }
}
