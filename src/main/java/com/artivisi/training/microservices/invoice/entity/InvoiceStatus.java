package com.artivisi.training.microservices.invoice.entity;

public enum InvoiceStatus {
    UNPAID,PAID,CANCELLED
}
