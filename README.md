# Cara mendapatkan Access Token

1. Buka URL authorization di browser [https://keycloak.artivisi.id/auth/realms/belajar/protocol/openid-connect/auth?scope=openid&response_type=code&client_id=yii2app&redirect_uri=http://example.com](https://keycloak.artivisi.id/auth/realms/belajar/protocol/openid-connect/auth?scope=openid&response_type=code&client_id=yii2app&redirect_uri=http://example.com). Nantinya kita akan diredirect sesuai dengan `redirect_uri` seperti ini :

    ```
    http://example.com/?session_state=b18ddb09-e0f9-4cfe-b9dc-92a3f5a7ced6&code=eb6c30fd-8903-4bef-ad62-d8a3714c8cc7.b18ddb09-e0f9-4cfe-b9dc-92a3f5a7ced6.e8d7ff2b-33f4-4be4-80a9-1a6be36b1deb
    ```

2. Copy `code` di URL redirect, tukarkan dengan `access_token`. Bisa dengan `curl` seperti ini

    ```
    curl --location --request POST 'https://keycloak.artivisi.id/auth/realms/belajar/protocol/openid-connect/token' \
    --header 'Authorization: Basic eWlpMmFwcDo3NDUyOGM0My01ZjM5LTRkYWMtODEyNC03MjFjZDJhMTU1ODM=' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'grant_type=authorization_code' \
    --data-urlencode 'code=eb6c30fd-8903-4bef-ad62-d8a3714c8cc7.b18ddb09-e0f9-4cfe-b9dc-92a3f5a7ced6.e8d7ff2b-33f4-4be4-80a9-1a6be36b1deb' \
    --data-urlencode 'redirect_uri=http://example.com' \
    --data-urlencode 'client_id=yii2app'
    ```

    atau dengan Postman seperti ini
   
    [![Postman Get Token](img/postman-get-token.png)](img/postman-get-token.png)

3. Setelah mendapatkan `access_token`, gunakan untuk mengakses resource server. Dengan curl seperti ini

    ```
    curl --location --request GET 'http://localhost:8080/api/invoice/' \
    --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJuUzVEWldLUUZiV3dyMndCNWxlWGV4M1Q0aUEyNmVyZzA1OEVHWDNhYmJBIn0.eyJleHAiOjE2MDk5ODc1MTgsImlhdCI6MTYwOTk4NzIxOCwiYXV0aF90aW1lIjoxNjA5OTg2ODYyLCJqdGkiOiJlODhmOGZhOC0zOTAyLTQ5YzEtYjFjMC1jZTJlYzRlODNiMDMiLCJpc3MiOiJodHRwczovL2tleWNsb2FrLmFydGl2aXNpLmlkL2F1dGgvcmVhbG1zL2JlbGFqYXIiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiNTRlNzRjYzktMTZiNy00MDYzLWFjNTEtYzc5YmQ4NDc1MjllIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoieWlpMmFwcCIsInNlc3Npb25fc3RhdGUiOiJiMThkZGIwOS1lMGY5LTRjZmUtYjlkYy05MmEzZjVhN2NlZDYiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHA6Ly9leGFtcGxlLmNvbSJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIGVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJVc2VyIDAwMSIsInByZWZlcnJlZF91c2VybmFtZSI6InVzZXIwMDEiLCJnaXZlbl9uYW1lIjoiVXNlciIsImZhbWlseV9uYW1lIjoiMDAxIiwiZW1haWwiOiJ1c2VyMDAxQHlvcG1haWwuY29tIn0.AP3QVc-XXJ1Ed2bTszGdTV1S1opFY59Zx82laRgfEHH1PMyx8UJrIZJvIqeOva13dEzrdSxkDVSbb7AV7W0fQNxkPxXRQI_rKAqHLk3Ny22n69wA_nqr-uGNlsaFCCztavG5gq9YNYdzIIBHhQAOtepCYeegSYXSz8RgugfYOA4dD9nf6LszxeI1O5oSeNvHINXRCuPCKY4CFj8MmcVYXem2sk4snQ8rdJOu5f_RbyOIsiD7O3EbndKQ8WK0WvdZvAjysDcYfz4GgH_qSGwxBir5QWOnv_bxwxwC4tno4z9ORSUS8EXRBDflRRQUjIrdCitqFysGjYH55br5vF_XXw' \
    --header 'Cookie: JSESSIONID=81D939E062D2F455DD944DA4F883C978'
    ```
    
    atau dengan Postman seperti ini
   
    [![Postman Bearer Token](img/postman-bearer-token.png)](img/postman-bearer-token.png)